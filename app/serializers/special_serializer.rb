include ActionView::Helpers::DateHelper
class SpecialSerializer < ActiveModel::Serializer
  attributes :id,:title,:text,:image,:time_gap
  has_one :business
  has_many :branches_specials
  def time_gap
  	distance_of_time_in_words_to_now(object.created_at) + ' ago'
  end
  def text
  	object.text.to_s
  end
end
