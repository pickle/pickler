class PunchedCardUserSerializer < ActiveModel::Serializer
  attributes :id,:current_stamp,:user_id,:used,:rewarded,:created_at,:stamp
  def stamp
    PunchedCardBusinessSerializer.new(object.punched_card_business, root: false)
  end
  def created_at
  	object.created_at.try(:strftime,"%d-%m-%Y")
  end
end
