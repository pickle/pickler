class AreaSerializer < ActiveModel::Serializer
  attributes :id,:name,:latitude,:longitude,:pincode
  has_one :city
end
