class BusinessForBranchSerializer < ActiveModel::Serializer
  attributes :id,:name,:logo,:keywords
  def keywords
  	object.keywords.limit(3).pluck(:name).join(', ')
  end
end
