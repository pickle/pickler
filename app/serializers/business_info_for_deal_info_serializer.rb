class BusinessInfoForDealInfoSerializer < ActiveModel::Serializer
  attributes :id,:name,:category,:logo,:terms_and_conditions,:redeem_steps
  has_one :category , serializer: CategoryInfoSerializer
  def terms_and_conditions
    object.terms_and_conditions.to_s
  end
  def redeem_steps
    object.redeem_steps.to_s
  end
end
