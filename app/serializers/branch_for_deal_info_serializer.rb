class BranchForDealInfoSerializer < ActiveModel::Serializer
  attributes :id,:name,:contacts
  has_one :area, serializer: AreaForDealInfoSerializer
  def contacts
    { "mobile" => object.mobile, "landline" => object.landline }
  end
  def rating
   ratings = 0
   if object.ratings.count > 0
    ratings = ((object.ratings.sum :rating) / object.ratings.count).round(1)
   end
   ratings
  end
  def no_of_deals
    object.deals.where("deals.buy_end_date > '#{Date.today}' and deals.buy_start_date < '#{Date.today}'").count
  end
  def is_favorite
    # We can use instance_options to get the user that we passed
    # to the render in the controller action
    object.is_favorite_for(serialization_options[:user])
  end
end
