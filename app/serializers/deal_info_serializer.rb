class DealInfoSerializer < ActiveModel::Serializer
  attributes :id,:name,:business,:price,:deal_price,:description,:available_days,:start_time,:end_time,:start_date,:end_date,:deal_type,:original_qty,:current_qty,:limited,:all_branches,:bought_count,:redeemed_count,:is_saved,:meta_text,:available_days_cons,:no_of_stamps,:redeem_type,:code_expiry_date,:buy_start_date,:buy_end_date,:highlights,:terms_and_conditions,:redeem_steps,:additional_info,:app_post,:state
  has_one :business, serializer: BusinessInfoForDealInfoSerializer
  has_many :branches_deals, serializer: BranchesDealForDealInfoSerializer
  has_many :images
  def available_days
    available_days_array = []
    days_of_week = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
    days_of_week.each_with_index do |value, index|
      if object.send(value.downcase[0..2])
        available_days_array.push(value)
      end
    end
    available_days_array
  end
  def is_saved
    # We can use instance_options to get the user that we passed
    # to the render in the controller action
    object.is_saved_for(serialization_options[:user])
  end
  def start_time
    Time.at(object.start_time).utc.strftime("%I:%M %p")
  end

  def end_time
    Time.at(object.end_time).utc.strftime("%I:%M %p")
  end
  def description
    object.description.to_s
  end
  def redeem_description
    object.redeem_description.to_s
  end
  def highlights
    object.highlights.to_s
  end
  def terms_and_conditions
    object.terms_and_conditions.to_s
  end
  def redeem_steps
    object.redeem_steps.to_s
  end
  def additional_info
    object.additional_info.to_s
  end
end