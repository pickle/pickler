class BranchInfoSerializer < ActiveModel::Serializer
  attributes :id,:name,:logo,:rating,:no_of_deals,:address,:area_id,:contacts,:no_of_people_who_rated,:state,:highlights,:description,:additional_info,:working_days,:latitude,:longitude,:is_favorite,:time_slots,:contact_email,:opened
  has_many :images
  has_many :attachments
  has_one :business ,serializer: BusinessInfoSerializer
  def contacts
  	{ "mobile" => object.mobile, "landline" => object.landline }
  end
  def no_of_people_who_rated
    object.ratings.count
  end
  def working_days
    available_days_array = []
    branch_available_days = object.branch_time_slots.pluck(:day).uniq
    days_of_week = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
    days_of_week.each_with_index do |value, index|
      if branch_available_days.include?(value.downcase[0..2])
        available_days_array.push(value)
      end
    end
    available_days_array
  end
  def is_favorite
    # We can use instance_options to get the user that we passed
    # to the render in the controller action
    object.is_favorite_for(serialization_options[:user])
  end
  def rating
   ratings = 0
   if object.ratings.count > 0
    ratings = ((object.ratings.sum :rating) / object.ratings.count).round(1)
   end
   ratings
  end
  def no_of_deals
    object.deals.where("deals.buy_start_date <= ? and deals.buy_end_date >= ?", Date.today, Date.today).count
  end
  def address
    object.address.to_s
  end
  def additional_info
    object.additional_info.to_s
  end
  def description
    object.description.to_s
  end
  def opened
    !object.branch_time_slots.where(day: Time.now.strftime('%a').downcase).where("start_time < #{Time.now.seconds_since_midnight.to_i} and end_time >  #{Time.now.seconds_since_midnight.to_i}").blank?
  end

end
