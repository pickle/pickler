class OrderSerializer < ActiveModel::Serializer
  attributes :id,:branch_id,:code,:code_status,:bought_at,:redeemed_at,:buy_type,:bought_by
  def branch_id
  	object.branches_deal.branch.id
  end
  def bought_at
  	object.bought_at.try(:strftime,"%d-%m-%Y")
  end
  def redeemed_at
  	object.redeemed_at.try(:strftime,"%d-%m-%Y")
  end
end
