class BusinessSerializer < ActiveModel::Serializer
  attributes :id,:name,:category,:logo,:keywords,:terms_and_conditions,:redeem_steps
  has_one :category , serializer: SubCategorySerializer
  def keywords
  	object.keywords.limit(3).pluck(:name).join(', ')
  end
  def terms_and_conditions
    object.terms_and_conditions.to_s
  end
  def redeem_steps
    object.redeem_steps.to_s
  end
end
