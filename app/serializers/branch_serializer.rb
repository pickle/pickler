class BranchSerializer < ActiveModel::Serializer
  attributes :id,:name,:rating,:no_of_deals,:is_favorite,:time_slots,:logo
  has_one :business,serializer: BusinessForBranchSerializer
  has_one :area,serializer: AreaWithIdNameSerializer
  def rating
   ratings = 0
   if object.ratings.count > 0
    ratings = ((object.ratings.sum :rating) / object.ratings.count).round(1)
   end
   ratings
  end
  def no_of_deals
    object.deals.where("deals.buy_start_date <= ? and deals.buy_end_date >= ?", Date.today, Date.today).count
  end
  def is_favorite
    # We can use instance_options to get the user that we passed
    # to the render in the controller action
    object.is_favorite_for(serialization_options[:user])
  end
  def time_slots
    days = { sun: "Sunday", mon: "Monday", tue: "Tuesday", wed: "Wednesday", thu: "Thursday", fri: "Friday", sat: "Saturday"}
    result = false
    object.branch_time_slots.where(day: Time.now.strftime('%A').downcase[0..2]).group_by(&:day).each do |day, time_values|
      result = time_values.map(&:time)
    end
    result
  end
end
