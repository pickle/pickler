class DealSerializer < ActiveModel::Serializer
  attributes :id,:name,:business,:price,:deal_price,:description,:available_days,:start_time,:end_time,:start_date,:end_date,:deal_type,:original_qty,:current_qty,:limited,:all_branches,:bought_count,:redeemed_count,:is_saved,:meta_text,:available_days_cons,:no_of_stamps,:redeem_type,:redeem_description,:code_expiry_date,:buy_start_date,:buy_end_date
  has_one :business, serializer: BusinessInfoForDealSerializer
  has_many :branches_deals
  has_many :images
  def available_days
    available_days_array = []
    days_of_week = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
    days_of_week.each_with_index do |value, index|
      if object.send(value.downcase[0..2])
        available_days_array.push(value)
      end
    end
    available_days_array
  end
  def is_saved
    # We can use instance_options to get the user that we passed
    # to the render in the controller action
    object.is_saved_for(serialization_options[:user])
  end
  def start_time
    Time.at(object.start_time).utc.strftime("%I:%M %p")
  end

  def end_time
    Time.at(object.end_time).utc.strftime("%I:%M %p")
  end
  def description
    object.description.to_s
  end
  def redeem_description
    object.redeem_description.to_s
  end
  
end
