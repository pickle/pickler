class CashDealCodeSerializer < ActiveModel::Serializer
  attributes :code,:code_status,:buy_type
  has_one :branches_deal

  def buy_type
    "cash"
  end
end
