class BranchesDealSerializer < ActiveModel::Serializer
  attributes :branch,:original_qty,:current_qty,:bought_count,:redeemed_count,:cash_bought
  has_one :branch, serializer: BranchesDealsInfoSerializer 
  def cash_bought
    # We can use instance_options to get the user that we passed
    # to the render in the controller action
    object.is_cash_bought_by(serialization_options[:user])
  end
end
