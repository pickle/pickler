class SubCategorySerializer < ActiveModel::Serializer
  attributes :id,:name,:image,:description,:keywords
  def description
  	object.description.to_s
  end
end
