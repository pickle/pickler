class AreaForDealInfoSerializer < ActiveModel::Serializer
  attributes :id,:name,:latitude,:longitude
  has_one :city, serializer: CityWithIdNameSerializer
end
