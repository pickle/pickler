class BusinessInfoSerializer < ActiveModel::Serializer
  attributes :id,:name,:category,:logo,:description,:website,:facebook,:keywords,:twitter,:contact_email
  has_one :category , serializer: SubCategorySerializer
  has_many :images
  def keywords
  	object.keywords.limit(3).pluck(:name).join(', ')
  end
  def description
    object.description.to_s
  end
end
