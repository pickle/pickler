class CategorySerializer < ActiveModel::Serializer
  attributes :id,:name,:number_of_businesses,:image,:keywords
  has_many :subcategories, serializer: SubCategorySerializer
  def number_of_businesses
  	object.businesses.count
  end
end
