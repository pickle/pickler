class PunchedCardBusinessSerializer < ActiveModel::Serializer
  attributes :id,:deal_id,:created_by,:stamp_code,:created_at
  def created_at
  	object.created_at.try(:strftime,"%d-%m-%Y")
  end
end
