class BranchesDealsInfoSerializer < ActiveModel::Serializer
  attributes :id,:name
  has_one :area, serializer: AreaWithIdNameSerializer
end
