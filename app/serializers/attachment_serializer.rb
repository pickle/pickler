class AttachmentSerializer < ActiveModel::Serializer
  attributes :id,:title,:images
  def images
  	object.images.pluck(:image)
  end
end
