class DealCodeSerializer < ActiveModel::Serializer
  attributes :code,:status,:code_status,:buy_type
  has_one :branches_deal

  def buy_type
    "online"
  end
end
