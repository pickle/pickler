class BranchesSpecialSerializer < ActiveModel::Serializer
  attributes :branch
  has_one :branch, serializer: ::BranchesDealsInfoSerializer 
end
