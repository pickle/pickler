class PunchedStampOrderSerializer < ActiveModel::Serializer
  attributes :id,:name,:business,:price,:deal_price,:description,:available_days,:start_time,:end_time,:start_date,:end_date,:deal_type,:original_qty,:current_qty,:limited,:all_branches,:bought_count,:redeemed_count,:app_buyable,:cash_buyable,:is_saved,:no_of_stamps,:available_days_cons,:redeem_type,:redeem_description,:code_expiry_date,:buy_start_date,:buy_end_date
  has_one :business
  has_many :branches_deals
  has_many :images
  has_many :stamped_punches, each_serializer: PunchedCardUserSerializer
  has_many :orders, each_serializer: OrderSerializer



  def available_days
    available_days_array = []
    days_of_week = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
    days_of_week.each_with_index do |value, index|
      if object.send(value.downcase[0..2])
        available_days_array.push(value)
      end
    end
    available_days_array
  end
  def branches_deals
    object.branches_deals
  end
  def is_saved
    # We can use instance_options to get the user that we passed
    # to the render in the controller action
    object.is_saved_for(serialization_options[:user])
  end

  def orders
    result = Array.new
    object.branches_deals.each do |bd|
      bd.send("deal_codes").where(bought_by: serialization_options[:user_id], code_status: serialization_options[:deal_code_status]).each do |dc|
        result << dc
      end
    end
    result
  end
  
  def start_time
    Time.at(object.start_time).utc.strftime("%I:%M %p")
  end

  def end_time
    Time.at(object.end_time).utc.strftime("%I:%M %p")
  end

  def stamped_punches
    output = []
    if(serialization_options[:used])
      output = object.punched_card_users.where(user_id: serialization_options[:user_id],used: serialization_options[:used])
      deal_code = object.punched_card_users.where(user_id: serialization_options[:user_id]).last.punched_card_business.stamp_code
      deal_code_object = DealCode.find_by_code(deal_code)
      if deal_code_object
        output = object.punched_card_users.where(user_id: serialization_options[:user_id]) if deal_code_object.redeemed?
      end
    else
      output = object.punched_card_users.where(user_id: serialization_options[:user_id],used: serialization_options[:used])
    end
    output
  end


  def description
    object.description.to_s
  end
  def redeem_description
    object.redeem_description.to_s
  end

end
