class CashDealCode < ActiveRecord::Base
  belongs_to :branches_deal
  

  enum status: [ :active, :inactive ]
  enum code_status: [ :unredeemed, :redeemed]
  enum buy_type: [ :online, :cash]
  

  validates_presence_of :branches_deal, :message => "Enter valid branch and deal id"
end
