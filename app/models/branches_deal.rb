class BranchesDeal < ActiveRecord::Base

  searchkick locations: ["location"], batch_size: 200

  belongs_to :branch
  belongs_to :deal
  has_many :deal_codes
  has_many :cash_deal_codes
  after_save :sync_deals
  
  def sync_deals
    @deal = self.deal
    if @deal.limited && !@deal.all_branches
      @deal.original_qty = @deal.branches_deals.sum :original_qty 
      @deal.current_qty = @deal.branches_deals.sum :current_qty 
    end
    if @deal.limited && @deal.all_branches && self.bought_count_changed?
      @deal.decrement(:current_qty)
    end
    @deal.bought_count = @deal.branches_deals.sum :bought_count 
    @deal.redeemed_count = @deal.branches_deals.sum :redeemed_count 
    @deal.save 
  end

  def should_index?
    # senthil: 2016jan18 removing state from ES
    #state &&
    deal.present? && branch.present? && deal.end_date.present? && deal.end_date >= Date.today && (deal.deleted != true) && (branch.deleted != true)
  end

  def search_data
    {
      deal_id: deal_id,
      branch_id: branch_id,
      title: deal.name,
      app_post: deal.app_post,
      state: deal.state,
      start_date: deal.start_date,
      end_date: deal.end_date,
      categories: get_all_types_of_categories,
      location: [branch.latitude, branch.longitude]
    }
  end

  def get_all_types_of_categories
    categories = Array.new
    category_id = self.branch.business.category_id
    categories << category_id
    categories += Category.where(parent_id: category_id).ids
    categories << deal.sub_category_id if deal.sub_category_id
    categories.uniq
  end

  def self.custom(keyword, categories, latitude, longitude, radius, page, per_page)
    page ||= 1
    per_page ||= 15
    radius ||= 5
    keyword = keyword.present? ? keyword : '*'
    condition = Hash.new
    if latitude.present? and longitude.present?
      condition = build_distance_query(latitude, longitude, radius)
      order = {_geo_distance: {location: "#{latitude},#{longitude}", order: "asc", unit: "km"} }
    end
    condition = condition.merge({app_post: true, state: true, start_date: {lte: Date.today}, end_date: {gte: Date.today} })
    categories = categories.split(',') if categories.present?
    condition.merge!({categories: categories}) if categories.present?
    fields = [:title]
    if order.present? 
      result = search( keyword, fields: fields, where: condition, order: order, page: page, per_page: per_page, load: false)
    else
      result = search( keyword, fields: fields, where: condition, page: page, per_page: per_page, load: false)
    end
    result
  end

  def is_cash_bought_by(user)
    user.cash_bought_branch_ids.include?(id) 
  end

  private

  def self.build_distance_query(latitude, longitude, radius)
    {location: {near: [latitude, longitude], within: radius.to_s+"km"}}
  end

end
