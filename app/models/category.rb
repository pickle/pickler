class Category < ActiveRecord::Base
	has_many :businesses
	has_many :deals
  has_many :subcategories, class_name: "Category", foreign_key: "parent_id", dependent: :destroy
  has_many :branches,through: :businesses
  default_scope { where(deleted: false) }

  #after_initialize :default_values

  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end

  def default_values 
    self.description ||= ""  
  end
end
