class DealCode < ActiveRecord::Base
  belongs_to :branches_deal
 
  enum status: [ :active, :inactive ]
  enum code_status: [ :unredeemed, :redeemed]
  enum buy_type: [ :online, :cash]

  validates_presence_of :branches_deal, :message => "Enter valid branch and deal id"

  after_save :sync_branch_deals
  
  def sync_branch_deals
    @branches_deal = self.branches_deal
    if self.code_status_changed?
      if self.redeemed?
        @branches_deal.increment(:redeemed_count)
      else
        @branches_deal.increment(:bought_count)
        @branches_deal.decrement(:current_qty) if @branches_deal.current_qty?
      end
    end
    @branches_deal.save
  end
end
