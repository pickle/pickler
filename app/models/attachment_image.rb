class AttachmentImage < ActiveRecord::Base
  belongs_to :attachment

  default_scope { where(deleted: false) }
  
  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end
end
