class Area < ActiveRecord::Base
  
	has_many :branches
	belongs_to :city

	validates_presence_of :city, :message => "Enter valid city id" 

	default_scope { where(deleted: false) }

	def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
	end
end
