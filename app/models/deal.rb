class Deal < ActiveRecord::Base


  belongs_to :sub_category, class_name: 'Category'
  belongs_to :business

  has_many :branches_deals
  has_many :punched_card_businesses
  has_many :punched_card_users, through: :punched_card_businesses
  has_many :images, as: :resource, dependent: :destroy
  has_many :branches, through: :branches_deals

  validates_presence_of :business, :message => "Enter valid buinsess id"
  validates :name,:description, :presence => true
  validates :deal_price,:presence => true, numericality: {only_float: true},:if => lambda {|u| deal_type != 'info'}
  validates :price,:presence => true, numericality: {only_float: true},:if => lambda {|u| !['punched_cards','info'].include?(deal_type) }
  validate :validate_deal

  accepts_nested_attributes_for :images

  default_scope { where(deleted: false) }

  #after_initialize :default_values
  # Experiment
  searchkick  locations: ["location"], batch_size: 200
  # Experiment
  after_save :elasticsearh_reindex

  enum redeem_type: [ :date, :time, :working_hours, :description ]
  

  # ====== Used in pickle dashboard app =============
  # ==== Senthil: 2016mar02 =========================
  def seconds_to_time(t)
    Time.at(t).utc.strftime("%I:%M%P")
  end

  def deal_start_time=(i)
    self.start_time = i
  end

  def deal_end_time=(i)
    self.end_time = i
  end

  def deal_start_time
    start_time.nil? ? start_time : seconds_to_time(start_time)
  end

  def deal_end_time
    end_time.nil? ? end_time : seconds_to_time(end_time)
  end

  
  # ==== Senthil: 2016mar02 =========================


  def validate_deal
    if app_post && !deleted
      errors.add(:branches, "A business should have at least one or more branches") if ((self.business.branches.count == 0) rescue false)
      errors.add(:terms_and_conditions, "Please enter Terms and Conditions before publishing the app") if terms_and_conditions.blank?
      errors.add(:redeem_steps, "Please enter Steps to redeem before publishing the app") if redeem_steps.blank?
    end
  end

  def elasticsearh_reindex
    self.branches_deals.each do |branch_deal|
      branch_deal.reindex
    end
  end

  # Experiment
  def search_data
    {
      title: name,
      app_post: app_post,
      state: state,
      buy_start_date: buy_start_date,
      buy_end_date: buy_end_date,
      categories: get_all_types_of_categories,
      location: get_all_lat_and_long
    }
  end

  def self.custom(keyword, categories, latitude, longitude, radius, page, per_page)
    page ||= 1
    per_page ||= 15
    radius ||= 5
    keyword = keyword.present? ? keyword : '*'
    condition = Hash.new
    if latitude.present? and longitude.present?
      condition = build_distance_query(latitude, longitude, radius)
      order = {_geo_distance: {location: "#{latitude},#{longitude}", order: "asc", unit: "km"} }
    end
    condition = condition.merge({app_post: true, state: true, buy_start_date: {lte: Date.today}, buy_end_date: {gte: Date.today} })
    categories = categories.split(',') if categories.present?
    condition.merge!({categories: categories}) if categories.present?
    fields = [:title]
    if order.present? 
      result = search( keyword, fields: fields, where: condition, order: order, page: page, per_page: per_page, load: false)
    else
      result = search( keyword, fields: fields, where: condition, page: page, per_page: per_page, load: false)
    end
    result
  end

  def should_index?
    branches_deals.present? && check_presence_of_lat_and_log && buy_end_date.present? && buy_end_date >= Date.today && (deleted != true)
  end

  def check_presence_of_lat_and_log
    self.branches.each do |branch|
      if (branch.deleted != true) and branch.business.present? and branch.latitude.present? and branch.longitude.present?
        return true
      end
    end
    false
  end

  def get_all_types_of_categories
    categories = Array.new
    categories << self.sub_category_id if self.sub_category_id
    self.branches.each do | branch |
      category_id = branch.business.category_id
      categories << category_id
      categories += Category.where(parent_id: category_id).ids
    end
    categories.uniq
  end

  def get_all_lat_and_long
    location = Array.new
    self.branches.each do |branch|
      if branch.latitude.present? and branch.longitude.present?
        location << [branch.latitude, branch.longitude] 
      end
    end
    location
  end
  # Experiment

  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end

  def is_saved_for(user)
    # The user's deal_ids should already be populated
    # since we do the eager-loading of them in the controller
    user.favorited_deal_ids.include?(id)
  end

  #methods for filtering

  def self.start_date_eq(dt)
    where('start_date = ?',dt)
  end

  def self.app_post_eq(value)
    where('app_post = ?',value)
  end

  def self.state_eq(value)
    where('deals.state = ?',value)
  end

  def self.end_date_eq(dt)
    where('end_date = ?',dt)
  end

  def self.end_date_lteq(dt)
    where('end_date <= ?',dt)
  end

  def self.start_date_gteq(dt)
    where('start_date >= ?',dt)
  end

  def self.end_date_gteq(dt)
    where('end_date >= ?',dt)
  end

  def self.start_date_lteq(dt)
    where('start_date <= ?',dt)
  end

  def self.buy_start_date_eq(dt)
    where('buy_start_date = ?',dt)
  end

  def self.buy_end_date_eq(dt)
    where('buy_end_date = ?',dt)
  end

  def self.buy_end_date_lteq(dt)
    where('buy_end_date <= ?',dt)
  end

  def self.buy_start_date_gteq(dt)
    where('buy_start_date >= ?',dt)
  end

  def self.buy_end_date_gteq(dt)
    where('buy_end_date >= ?',dt)
  end

  def self.buy_start_date_lteq(dt)
    where('buy_start_date <= ?',dt)
  end

  def self.business_id_eq(id)
    where(get_condition(id,'deals.business_id'))
  end

  def self.branch_id_eq(id)
    where(get_condition(id,'branches.id'))
  end

  def self.category_id_eq(id)
    where(get_condition(id,'businesses.category_id'))
  end

  def self.available_days_eq(days)
    where(get_days_condition(days))
  end

  def self.get_days_condition(days)
    days.split(',').map  { |day| 'deals.'+day+'=1' }.join(' or ')
  end

  def self.get_condition(ids,column_name)
    ids.split(',').map  { |id| column_name+'='+id }.join(' or ')
  end

  def business_name
    business.try(:name)
  end

  def business_name=(name)
    self.business = Business.find_by(name: name) if name.present?
  end

  def default_values
    self.highlights ||= ""
    self.terms_and_conditions ||= ""
    self.redeem_steps ||= ""
    self.additional_info ||= ""
  end



  def available_days_cons
    available_days_array = []
    output = []
    days_of_week = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
    days_of_week.each_with_index do |value, index|
      if self.send(value.downcase[0..2])
        available_days_array.push(index)
      end
    end
    days_count = available_days_array.length
    available_days_array = available_days_array.sort.each_with_index.chunk {|x,i| x-i }.map{ |diff, pairs|
      pairs.first[0].to_s+'-'+pairs.last[0].to_s 
    }
    available_days_array.each_with_index do |value, index|
      if value
        days_of_week_array = value.split('-')
        if(days_of_week_array[0]==days_of_week_array[1])
          output.push(days_of_week[days_of_week_array[0].to_i])
        elsif((days_of_week_array[0].to_i + 1)== days_of_week_array[1].to_i)
          output.push(days_of_week[days_of_week_array[0].to_i])
          output.push(days_of_week[days_of_week_array[1].to_i])
        else
          output.push(days_of_week[days_of_week_array[0].to_i] + '-' + days_of_week[days_of_week_array[1].to_i])
        end
      end
    end
    output = ['All days'] if days_count==7
    output
  end
  
  # Experiment
  private

  def self.build_distance_query(latitude, longitude, radius)
    {location: {near: [latitude, longitude], within: radius.to_s+"km"}}
  end
  # Experiment
end
