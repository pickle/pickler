class PunchedCardUser < ActiveRecord::Base
  belongs_to :punched_card_business, foreign_key: :stamp_id
  belongs_to :user
end
