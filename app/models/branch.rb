require 'time'

class Branch < ActiveRecord::Base

  searchkick locations: ["location"], batch_size: 200, word_start: [:business_title, :keywords] #, word_middle: [:business_title, :keywords]

  belongs_to :business
  belongs_to :area
  has_many :ratings, dependent: :destroy
  has_many :branches_deals
  has_many :branches_specials
  has_many :deals, through: :branches_deals
  has_many :specials, through: :branches_specials
  has_many :images, as: :resource, dependent: :destroy
  has_many :attachments, as: :attachable, dependent: :destroy
  has_many :branch_favorites
  has_many :branch_time_slots


  validates_presence_of :area, :message => "Enter valid area id" 
  validates_presence_of :business, :message => "Enter valid buinsess id"
  validates :business_id, :presence => true
  validate :validate_branch

  accepts_nested_attributes_for :images
  accepts_nested_attributes_for :attachments

  after_save :elasticsearh_reindex

  default_scope { where(deleted: false) }

  #after_initialize :default_values

  # ====== Used in pickle dashboard app =============
  # ==== Senthil: 2016feb10 =========================
  def seconds_to_time(t)
    Time.at(t).utc.strftime("%I:%M%P")
  end

  def branch_open_time=(i)
    self.open_time = i
  end

  def branch_close_time=(i)
    self.close_time = i
  end

  def branch_open_time
    open_time.nil? ? open_time : seconds_to_time(open_time)
  end

  def branch_close_time
    close_time.nil? ? close_time : seconds_to_time(close_time)
  end
  # ==== Senthil: 2016feb10 =========================

  def validate_branch
    
    if landline.blank? & mobile.blank?
      errors.add(:contact, "Please enter either mobile or landline")
    end
  end

  def should_index?
    # senthil: 2016jan19: removing state true from index
    #state &&
    latitude.present? && longitude.present? && business.present? && (deleted != true)
  end

  def search_data
    {
      business_id: business.id,
      business_title: business.name,
      name: name,
      keywords: business.keywords.map(&:name),
      categories: get_all_types_of_categories,
      deal_ids: branches_deals.map(&:deal_id),
      location: [latitude, longitude]
    }
  end

  def get_all_types_of_categories
    categories = Array.new
    category_id = self.business.category_id
    categories = Category.where(parent_id: category_id).ids
    categories << category_id
    categories.uniq
  end

  def self.suggestions(keyword, categories, latitude, longitude, radius, page, per_page)
    page ||= 1
    per_page ||= 15
    radius ||= 5
    business = Hash.new
    condition = Hash.new
    condition = build_distance_query(latitude, longitude, radius) if latitude.present? and longitude.present?
    categories = categories.split(',') if categories.present?
    condition.merge!({categories: categories}) if categories.present?
    result = search( 
                    keyword,
                    fields: [{business_title: :word_start}, {keywords: :word_start}],
                    where: condition,
                    misspellings: false,
                    page: page,
                    per_page: per_page,
                    load: false
                   )
    result.results.present? ? result.results.uniq { |i| i[:business_id] } : []
  end

  def self.list_neary_by(business_id, latitude, longitude, radius, page, per_page)
    page ||= 1
    per_page ||= 15
    radius ||= 5
    # cities = City.search "san", order: {_geo_distance: {location: "#{lat},#{lon}", order: "asc", unit: "mi"} }
    condition = Hash.new
    order = Hash.new
    if latitude.present? and longitude.present?
      condition = build_distance_query(latitude, longitude, radius) 
      order = {_geo_distance: {location: "#{latitude},#{longitude}", order: "asc", unit: "km"} }
    end
    condition.merge!({business_id: business_id}) if business_id.present?
    if order.present?
      results = search(where: condition, order: order, page: page, per_page: per_page, load: false)
    else
      results = search(where: condition, page: page, per_page: per_page, load: false)
    end
  end

  def self.custom(keyword, categories, latitude, longitude, radius, page, per_page)
    page ||= 1
    per_page ||= 15
    radius ||= 5
    keyword = keyword.present? ? keyword : '*'
    condition = Hash.new
    if latitude.present? and longitude.present?
      condition = build_distance_query(latitude, longitude, radius)
      order = {_geo_distance: {location: "#{latitude},#{longitude}", order: "asc", unit: "km"} }
    end
    categories = categories.split(',') if categories.present?
    condition.merge!({categories: categories}) if categories.present?
    fields = [:business_title, :keywords, :name]
    if order.present?
      result = search( keyword, fields: fields, where: condition, order: order, page: page, per_page: per_page, load: false)
    else
      result = search( keyword, fields: fields, where: condition, page: page, per_page: per_page, load: false)
    end
  end

  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end

  def default_values 
    self.description ||= "" 
    self.address ||= "" 
    self.additional_info ||= "" 
  end

  def is_favorite_for(user)
    # The user's deal_ids should already be populated
    # since we do the eager-loading of them in the controller
    user.favorited_branch_ids.include?(id)
  end

  def elasticsearh_reindex
    self.branches_deals.each do |branch_deal|
      branch_deal.reindex
    end
    self.branch_time_slots.each do |branch_time_slot|
      branch_time_slot.reindex
    end
    # Experiment
    self.deals.each do |deal|
      deal.reindex
    end
    # Experiment
  end

  def time_slots
    days = { sun: "Sunday", mon: "Monday", tue: "Tuesday", wed: "Wednesday", thu: "Thursday", fri: "Friday", sat: "Saturday"}
    result = {}
    self.branch_time_slots.group_by(&:day).each do |day, time_values|
      result[day] = time_values.map(&:time)
    end
    result = result.transform_keys{ |key| days[key.to_sym] }
    result
  end

  def self.created_at_eq(dt)
    where('created_at = ?',dt)
  end

  def self.created_at_lteq(dt)
    where('created_at <= ?',dt)
  end

   def self.created_at_gteq(dt)
    where('created_at >= ?',dt)
  end


  private

  def self.build_distance_query(latitude, longitude, radius)
    {location: {near: [latitude, longitude], within: radius.to_s+"km"}}
  end

end
