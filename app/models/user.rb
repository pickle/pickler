class User < ActiveRecord::Base
  validates :mobile, :presence => true
  has_many :branch_views
  has_many :recently_viewed_branches,-> { order('branch_views.created_at DESC').uniq }, through: :branch_views, source: :branch
  has_many :branch_favorites
  has_many :favorited_branches,-> { distinct }, through: :branch_favorites, source: :branch
  has_many :deal_favorites
  has_many :favorited_deals,-> { distinct }, through: :deal_favorites, source: :deal
  has_many :deal_codes, foreign_key: "bought_by"
  has_many :cash_deal_codes, foreign_key: "bought_by"
  has_many :cash_redeemed_codes,-> { where buy_type: "1"},class_name: "DealCode", foreign_key: "bought_by"
  has_many :cash_bought_codes,class_name: "CashDealCode", foreign_key: "bought_by"
  has_many :cash_bought_branches,-> { distinct },through: :cash_bought_codes,source: :branches_deal
  has_many :cash_redeemed_branches,-> { distinct },through: :cash_redeemed_codes,source: :branches_deal
  has_many :punched_card_businesses,through: :punched_card_users,foreign_key: :stamp_id
  has_many :punched_deals,through: :punched_card_businesses,foreign_key: :deal_id,source: :deal
  has_many :punched_card_users
  has_many :favorited_specials,-> {distinct} , through: :favorited_branches, source: :specials
end
