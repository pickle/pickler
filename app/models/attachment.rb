class Attachment < ActiveRecord::Base
  belongs_to :attachable, polymorphic: true
  has_many :images, dependent: :destroy, :class_name => "AttachmentImage"
  accepts_nested_attributes_for :images

  default_scope { where(deleted: false) }
  
  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end
end
