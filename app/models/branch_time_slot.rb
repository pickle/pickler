class BranchTimeSlot < ActiveRecord::Base
  belongs_to :branch
  def time
    [Time.at(start_time).utc.strftime("%I:%M %p"), Time.at(end_time).utc.strftime("%I:%M %p")].join('-')
  end	

  searchkick locations: ["location"], batch_size: 200

  def should_index?
    branch.present? && branch.latitude.present? && branch.longitude.present? && branch.business.present? && (branch.deleted != true)
  end

  def search_data
    {
      business_id: branch.business.id,
      business_title: branch.business.name,
      name: branch.name,
      keywords: branch.business.keywords.map(&:name),
      categories: branch.get_all_types_of_categories,
      deal_ids: branch.branches_deals.map(&:deal_id),
      location: [branch.latitude, branch.longitude],
      branch_id: branch_id,
      start_time: start_time,
      end_time: end_time,
      day: day
    }
  end

  def get_all_types_of_categories
    categories = Array.new
    category_id = self.branch.business.category_id
    categories = Category.where(parent_id: category_id).ids
    categories << category_id
    categories.uniq
  end

  def self.custom(keyword, categories, latitude, longitude, radius, page, per_page)
    page ||= 1
    per_page ||= 15
    radius ||= 5
    keyword = keyword.present? ? keyword : '*'
    condition = Hash.new
    if latitude.present? and longitude.present?
      condition = build_distance_query(latitude, longitude, radius)
      order = {_geo_distance: {location: "#{latitude},#{longitude}", order: "asc", unit: "km"} }
    end
    categories = categories.split(',') if categories.present?
    condition.merge!({categories: categories}) if categories.present?
    condition.merge!({day: Time.now.strftime('%A').downcase[0..2], start_time: {lt: Time.now.seconds_since_midnight.to_i}, end_time: {gt: Time.now.seconds_since_midnight.to_i} })
    fields = [:business_title, :keywords, :name]
    if order.present?
      result = search( keyword, fields: fields, where: condition, order: order, page: page, per_page: per_page, load: false)
    else
      result = search( keyword, fields: fields, where: condition, page: page, per_page: per_page, load: false)
    end
  end

  private

  def self.build_distance_query(latitude, longitude, radius)
    {location: {near: [latitude, longitude], within: radius.to_s+"km"}}
  end
end
