class Special < ActiveRecord::Base
  
  belongs_to :business
  has_many :branches_specials
  has_many :branches, through: :branches_specials

  default_scope { where(deleted: false) }

  #after_initialize :default_values

  def destroy
    self.deleted = true
    self.deleted_at = Time.now
    self.save
  end

  def default_values 
    self.text ||= ""  
  end
end
