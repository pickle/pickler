class PunchedCardBusiness < ActiveRecord::Base
	has_many :punched_card_users, foreign_key: :stamp_id
	belongs_to :deal
	belongs_to :business
end
