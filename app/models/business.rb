class Business < ActiveRecord::Base
  attr_accessor :add_keywords
  belongs_to :category
  has_many :branches, dependent: :destroy
  has_many :deals, dependent: :destroy
  has_many :images, as: :resource, dependent: :destroy
  has_many :businesses_keywords
  has_many :keywords, through: :businesses_keywords
  has_many :punched_card_businesses

  validates :name,:logo,:category_id, :presence => true
  accepts_nested_attributes_for :images

  default_scope { where(deleted: false) }

  after_save :search_index
  before_destroy :remove_index

  #after_initialize :default_values

  def remove_index
    if !self.branches.blank?
      self.branches.find_each do |i|
        Branch.searchkick_index.remove(i)
      end
    end
  end


  def search_index
    if !self.branches.blank? && (keywords.any? {|k| k.name_changed?} || name_changed? || category_id_changed?)
      self.keywords.find_each do |keyword|
        keyword.reindex
      end
      self.branches.find_each do |branch|
        branch.reindex
        if branch.branches_deals.present?
          branch.branches_deals.find_each do |branches_deal|
            branches_deal.reindex
          end
        end
      end
    end
  end

  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end

  def default_values
    self.description ||= ""
  end

  def self.autocomplete(term)
    where('LOWER(name) LIKE :term', term: "%#{term.downcase}%")
  end

end
