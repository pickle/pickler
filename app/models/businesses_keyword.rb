class BusinessesKeyword < ActiveRecord::Base
  belongs_to :keyword
  belongs_to :business


  validates_presence_of :keyword, :message => "Enter valid keyword id" 

  validates_uniqueness_of :business_id, :scope => :keyword_id
end