class Country < ActiveRecord::Base

  has_many :cities

  default_scope { where(deleted: false) }

  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end
end
