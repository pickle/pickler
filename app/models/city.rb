class City < ActiveRecord::Base
  belongs_to :country
  has_many :areas

  validates_presence_of :country, :message => "Enter valid country id" 

  default_scope { where(deleted: false) }

  def destroy
    run_callbacks :destroy do
      self.deleted = true
      self.deleted_at = Time.now
      self.save
    end
  end
end
