class Keyword < ActiveRecord::Base

  searchkick batch_size: 200, word_start: [:name]

  has_many :businesses_keywords
  has_many :businesses, through: :businesses_keywords

  before_validation :downcase_keyword
  after_save :elasticsearh_reindex_branch
  validates_uniqueness_of :name

  def elasticsearh_reindex_branch
    self.businesses.each do |business|
      business.branches.each do |branch|
        branch.branch_time_slots.each do |branch_time_slot|
          branch_time_slot.reindex
        end
        branch.reindex
      end
    end
  end

  def should_index?
    businesses.present?
  end

  def search_data
    {
      name: name
    }
  end

  def self.suggestions(keyword)
    keyword = keyword.present? ? keyword : '*'
    search(
           keyword, 
           misspellings: false,
           load: false
          )
  end

  def downcase_keyword
    self.name = self.name.downcase
  end
end