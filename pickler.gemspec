$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "pickler/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "pickler"
  s.version     = Pickler::VERSION
  s.authors     = ["beck03076"]
  s.email       = ["senthilkumar.hce@gmail.com"]
  s.summary     = "Summary of Pickler."
  s.description = "Description of Pickler."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails"
  s.add_dependency "active_model_serializers", "~> 0.9.3"
  s.add_dependency "searchkick", '~> 1.3'

  s.add_development_dependency "sqlite3"
end
