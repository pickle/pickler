module Pickler
  class Engine < ::Rails::Engine
    require 'searchkick'
    require 'active_model_serializers'
    isolate_namespace Pickler
  end
end
