searchkick_config_file = File.join(Pickler::Engine.root,'config','searchkick.yml')
raise "#{searchkick_config_file} is missing!" unless File.exists? searchkick_config_file
searchkick_config = YAML.load_file(searchkick_config_file)[Rails.env].symbolize_keys

# Reference - https://github.com/ankane/searchkick/blob/master/lib/searchkick.rb (line: 39)
Searchkick.client = Elasticsearch::Client.new(hosts: [searchkick_config[:elasticsearch]["host"]], retry_on_failure: true, transport_options: {request: {timeout: 250}})